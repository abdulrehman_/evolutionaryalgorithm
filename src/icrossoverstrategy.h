#ifndef ICROSSOVERSTRATEGY_H
#define ICROSSOVERSTRATEGY_H

#include "individual.h"
#include <vector>

class ICrossoverStrategy
{
public:
    virtual std::vector<Individual> Crossover(std::vector<Individual> &parents) = 0;
    virtual ~ICrossoverStrategy(){}
};

#endif // ICROSSOVERSTRATEGY_H
