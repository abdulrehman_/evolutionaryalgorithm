#include "htmlwriter.h"



HTMLWriter::HTMLWriter(std::string path, const std::vector<std::pair<Individual,double>> &data):
    file(path, std::ios_base::out | std::ios_base::trunc),
    data(data)
{

}

HTMLWriter::~HTMLWriter()
{
    file.close();
}

void HTMLWriter::WriteHeader()
{
    file << "<!DOCTYPE HTML>"
            "<html>"
            "<head>"
            "<title>Evolutionary Algorithm Graph</title>"

            "<style>"
            "body {font: 10pt arial;}"
            "</style>"

            "<script type=\"text/javascript\" src=\"vis.js\"></script>"
            "<script type=\"text/javascript\">";
}

void HTMLWriter::WriteContent(int contentId)
{
    file << "function drawVisualization" << contentId << "(){"
         <<  "var data = new vis.DataSet();";
    for ( unsigned int i = 0; i < data.size(); i++){

         file << "data.add({"
              << "x: " << data[i].first.getX() << ","
              << "y: " << data[i].first.getY() << ","
              << "z: " << data[i].second << ","
              << "style: " << data[i].second
              << "});" << std::endl;
    }

    file <<   "var options = {"
              "width:  '600px',"
              "height: '600px',"
              "style: 'dot-color',"
              "showPerspective: true,"
              "showGrid: true,"
              "showShadow: false,"
              "keepAspectRatio: true,"
              "verticalRatio: 0.5"
            "};"
            "var container = document.getElementById('mygraph" << contentId <<"');"
        //    "document.getElementById('best" << number << "').innerHTML = \"Best: "<< individuals[0].second <<"\";"
        //    "document.getElementById('avg" << number  << "').innerHTML = \"Average: " << totalFitness/individuals.size() <<"\";"
            "graph3d = new vis.Graph3d(container, data, options);"
          "}";
}

void HTMLWriter::WriteFooter()
{
    file << "function drawVisualization(){";
    for ( int i = 0; i < data.size(); i++){
      file << "drawVisualization" << i <<"();";
    }
    file <<  "}</script>"
            "</head>"
            "<body onload=\"drawVisualization();\">";
      for ( int i = 0; i < data.size(); i++){
        file << "<div id=\"mygraph" << i <<"\"></div>"
             << "<div> "
          //   << "<h2 id=\"best"<< i <<"\">"
          //   << "</h2>"
          //   << "<h2 id=\"avg" << i <<"\">"
          //   << "</h2>"
             << "</div>";
      }
       file << "</body>"
               "</html>";
}

void HTMLWriter::setData(std::vector<std::pair<Individual, double>> &value)
{
    data = value;
}
