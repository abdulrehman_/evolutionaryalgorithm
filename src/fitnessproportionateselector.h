#ifndef FITNESSPROPORTIONATESELECTOR_H
#define FITNESSPROPORTIONATESELECTOR_H

#include "iselector.h"
#include <vector>
#include <map>
#include "interval.h"

class FitnessProportionateSelector :public ISelector
{
    std::map<Interval, int, IntervalComparator> probabilityIndex;
    std::vector<std::pair<Individual, double>> data;
    double totalFitness;
public:
    FitnessProportionateSelector();

    // ISelector interface
public:
    void Calculate();
    std::vector<Individual> Select();
    std::vector<std::pair<Individual, double> > getData() const;
    void setData(const std::vector<std::pair<Individual, double> > &value);
};

#endif // FITNESSPROPORTIONATESELECTOR_H
