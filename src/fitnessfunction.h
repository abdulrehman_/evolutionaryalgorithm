#ifndef FITNESSFUNCTION_H
#define FITNESSFUNCTION_H

#include "individual.h"

class FitnessFunction
{

public:
    enum Function{ X2Y2 = 0, RosenBrock = 1, HimmelBlau = 2};
    Function function;
    FitnessFunction();
    double Calculate(Individual chromosome);
    void setFunction(const Function &value);
    Function getFunction() const;
};

#endif // FITNESSFUNCTION_H
