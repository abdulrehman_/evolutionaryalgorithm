#ifndef ONEPOINTCROSSOVER_H
#define ONEPOINTCROSSOVER_H

#include "icrossoverstrategy.h"

class OnePointCrossover : public ICrossoverStrategy
{
public:
    OnePointCrossover();

    // ICrossoverStrategy interface
public:
    std::vector<Individual> Crossover(std::vector<Individual> &parents);
};

#endif // ONEPOINTCROSSOVER_H
