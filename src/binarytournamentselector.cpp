#include "binarytournamentselector.h"

BinaryTournamentSelector::BinaryTournamentSelector()
{

}

void BinaryTournamentSelector::setData(const std::vector<std::pair<Individual, double> > &value)
{
        data = value;
}

void BinaryTournamentSelector::Calculate()
{
    std::random_device rd;
    std::mt19937_64 ed(rd());
    std::uniform_int_distribution<> randomIndex(0, data.size()-1);
    tournamentMembers.clear();
        for ( int i = 0; i < data.size()/3; i++){
           tournamentMembers.push_back(data[randomIndex(ed)]);
        }
        std::sort(tournamentMembers.begin(), tournamentMembers.end(), [] (const std::pair<Individual, double> &a, const std::pair<Individual, double> &b) -> bool
        {
            return a.second > b.second; }
        );
}

std::vector<Individual> BinaryTournamentSelector::Select()
{
    std::vector<Individual> parents;
    parents.push_back(tournamentMembers[0].first);
    parents.push_back(tournamentMembers[1].first);

    return parents;
}
