#include "algorithmsettingdialog.h"
#include "ui_algorithmsettingdialog.h"

AlgorithmSettingDialog::AlgorithmSettingDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AlgorithmSettingDialog)
{
    ui->setupUi(this);

    settings.beginGroup("Mutation");
     ui->checkBoxIsAllow->setChecked(settings.value("isAllow").toBool());
     ui->comboBoxMutationType->setCurrentIndex(settings.value("type").toInt());
     ui->doubleSpinBoxMutationProbability->setValue(settings.value("probability").toDouble());
     ui->doubleSpinBoxMutationMin->setValue(settings.value("minimumValue").toDouble());
     ui->doubleSpinBoxMutationMax->setValue(settings.value("maximumValue").toDouble());
     settings.endGroup();

    settings.beginGroup("Crossover");
      ui->comboBoxCrossoverType->setCurrentIndex(settings.value("type").toInt());
      ui->doubleSpinBoxCrossoverBreedRate->setValue(settings.value("breedRate").toDouble());
    settings.endGroup();

    settings.beginGroup("FitnessFunction");
        ui->comboBoxFitnessFunction->setCurrentIndex(settings.value("index").toInt());
    settings.endGroup();

    settings.beginGroup("Population");
    ui->comboBoxSelectionType->setCurrentIndex(settings.value("selectionType").toInt());
    ui->spinBoxPopulation->setValue(settings.value("size").toInt());
    ui->spinBoxGeneration->setValue(settings.value("generation").toInt());
    ui->spinBoxIteration->setValue(settings.value("iteration").toInt());
    settings.endGroup();

    settings.beginGroup("Limit");
       ui->spinBoxMinX->setValue(settings.value("minX").toInt());
       ui->spinBoxMinY->setValue(settings.value("minY").toInt());
       ui->spinBoxMaxX->setValue(settings.value("maxX").toInt());
       ui->spinBoxMaxY->setValue(settings.value("maxY").toInt());
    settings.endGroup();

}

AlgorithmSettingDialog::~AlgorithmSettingDialog()
{
    delete ui;
}

void AlgorithmSettingDialog::GetStarted()
{
     int i = exec();
     if ( Accepted){
     settings.beginGroup("Mutation");
      settings.setValue("isAllow", ui->checkBoxIsAllow->isChecked());
      settings.setValue("type", ui->comboBoxMutationType->currentIndex());
      settings.setValue("probability", ui->doubleSpinBoxMutationProbability->value());
      settings.setValue("minimumValue", ui->doubleSpinBoxMutationMin->value());
      settings.setValue("maximumValue", ui->doubleSpinBoxMutationMax->value());
     settings.endGroup();

     settings.beginGroup("Crossover");
        settings.setValue("type", ui->comboBoxCrossoverType->currentIndex());
        settings.setValue("breedRate", ui->doubleSpinBoxCrossoverBreedRate->value());
     settings.endGroup();

     settings.beginGroup("FitnessFunction");
        settings.setValue("index", ui->comboBoxFitnessFunction->currentIndex());
     settings.endGroup();

     settings.beginGroup("Population");
        settings.setValue("selectionType", ui->comboBoxSelectionType->currentIndex());
        settings.setValue("size", ui->spinBoxPopulation->value());
        settings.setValue("generation", ui->spinBoxGeneration->value());
        settings.setValue("iteration", ui->spinBoxIteration->value());
     settings.endGroup();

     settings.beginGroup("Limit");
        settings.setValue("minX", ui->spinBoxMinX->value());
        settings.setValue("minY", ui->spinBoxMinY->value());
        settings.setValue("maxX", ui->spinBoxMaxX->value());
        settings.setValue("maxY", ui->spinBoxMaxY->value());
     settings.endGroup();
    // settings.sync();
     }
}
