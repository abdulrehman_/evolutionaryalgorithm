#ifndef SWAPMUTATOR_H
#define SWAPMUTATOR_H

#include "imutator.h"

class SwapMutator : public IMutator
{
public:
    SwapMutator();

    // IMutator interface

public:
    void Mutate(std::vector<Individual> &offSpring);
};

#endif // SWAPMUTATOR_H
