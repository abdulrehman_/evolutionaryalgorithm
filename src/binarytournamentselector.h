#ifndef BINARYTOURNAMENTSELECTOR_H
#define BINARYTOURNAMENTSELECTOR_H

#include "iselector.h"
#include <vector>
#include <algorithm>

class BinaryTournamentSelector : public ISelector
{
public:
    BinaryTournamentSelector();
    std::vector<std::pair<Individual, double>> data;
    std::vector<std::pair<Individual, double>> tournamentMembers;
    // ISelector interface
public:
    void setData(const std::vector<std::pair<Individual, double> > &value);
    void Calculate();
    std::vector<Individual> Select();
};

#endif // BINARYTOURNAMENTSELECTOR_H
