#ifndef PARTICLE_H
#define PARTICLE_H

#include <vector>

class Particle
{

public:
    std::vector<double> v;
    std::vector<double> present;
    std::vector<double> lbest;

    Particle();

    std::vector<double> getV() const;
    void setV(const std::vector<double> &value);

    std::vector<double> getPresent() const;
    void setPresent(const std::vector<double> &value);

    std::vector<double> getLbest() const;
    void setLbest(const std::vector<double> &value);
};

#endif // PARTICLE_H
