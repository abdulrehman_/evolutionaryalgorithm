#ifndef INTERVAL_H
#define INTERVAL_H

struct Interval
{
    Interval() : start(), end() {}
    Interval(double start, double end) : start(start), end(end) {}
    double start;
    double end;
};
struct IntervalComparator{
bool operator()(const Interval& lhs, const Interval& rhs)
 {
    if ( lhs.end < rhs.start)
        return true;
    else
        return false;
 }
};
#endif // INTERVAL_H
