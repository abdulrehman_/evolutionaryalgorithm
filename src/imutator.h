#ifndef MUTATOR_H
#define MUTATOR_H

#include <vector>
#include <individual.h>


class IMutator
{
public:


     virtual void Mutate(std::vector<Individual> &offSpring) = 0;
     virtual ~IMutator(){

    }
};

#endif // MUTATOR_H
