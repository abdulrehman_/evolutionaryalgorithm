#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "evolutionaryalgorithm.h"
#include "algorithmsettingdialog.h"
#include "insertmutator.h"
#include "swapmutator.h"
#include "onepointcrossover.h"
#include "binarytournamentselector.h"
#include "rankbasedselector.h"
#include "qcustomplot.h"
#include "fitnessproportionateselector.h"
#include "iselector.h"
#include "swarm.h"
#include <QStringList>
#include <QFileDialog>
#include <QMessageBox>
#include <QTextStream>
#include <QFile>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
  //  EvolutionaryAlgorithm *ea;
    QVector<double> generations;
    QVector<double> iterations;

    int iterationSize;
    int generationSize;

    QVector<double> fitnessAvg;
    QVector<double> fitnessBest;

    QVector<double> psoFitnessAvg;
    QVector<double> psoFitnessBest;

    QVector<QVector<double>> fitnessAvgCollection;
    QVector<QVector<double>> fitnessBestCollection;


    QVector<double> fitnessAvgAvg;
    QVector<double> fitnessAvgBest;

    QVector<double> psoFitnessAvgAvg;
    QVector<double> psoFitnessAvgBest;

    QVector<QVector<double>> psoFitnessAvgCollection;
    QVector<QVector<double>> psoFitnessBestCollection;

    int selectedGraph;

    Ui::MainWindow *ui;
    QSettings settings;

public slots:
    void Execute();
    void Next();
    void Previous();
    void algorithmSettings();
    void Open3DGraphOnBrowser();
    void ShowHideLegend(bool value);
    void EnablePSO(bool value);
    void Export();

};

#endif // MAINWINDOW_H
