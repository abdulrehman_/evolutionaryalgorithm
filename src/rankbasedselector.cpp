#include "rankbasedselector.h"

RankBasedSelector::RankBasedSelector()
{

}

void RankBasedSelector::setData(const std::vector<std::pair<Individual, double>> &value)
{
    data = value;
}

void RankBasedSelector::Calculate()
{
    totalFitness = 0;
    probabilityIndex.erase(probabilityIndex.begin(), probabilityIndex.end());

    for ( unsigned int i = 0; i < data.size(); i++){
        totalFitness += data[i].first.getX();
    }
    assert( totalFitness != 0);

    double interval = 0.0;
    for (unsigned int i = 0; i < data.size(); i++){
       probabilityIndex[Interval(interval, interval + data[i].second/totalFitness)] = i;
        interval += data[i].first.getX()/totalFitness + 0.000001;
    }
}

std::vector<Individual> RankBasedSelector::Select()
{
    std::random_device rd;
    std::mt19937_64 ed(rd());

    std::uniform_real_distribution<> dist(0, 1);

    double random = dist(ed);
    Interval p1(random, random);
    std::vector<Individual> parents;

    parents.reserve(2);
    parents.push_back(data[probabilityIndex[p1]].first);
    random = dist(ed);

    Interval p2(random, random);

    parents.push_back(data[probabilityIndex[p2]].first);

    return parents;
}
