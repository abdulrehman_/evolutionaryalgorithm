#include "swarm.h"

std::vector<Particle> Swarm::getParticles() const
{
    return particles;
}

void Swarm::setParticles(const std::vector<Particle> &value)
{
    particles = value;
}

std::vector<double> Swarm::getGbest() const
{
    return gbest;
}

void Swarm::setGbest(const std::vector<double> &value)
{
    gbest = value;
}

int Swarm::getSwarmSize() const
{
    return swarmSize;
}

Swarm::Swarm()
{

}

Swarm::Swarm(int swarmSize):
    swarmSize(swarmSize), gbest(2)
{

     c1 = 2;
     c2 = 2;

    gbest[0] = 0;
    gbest[1] = 0;


}

void Swarm::
Execute()
{
  particles.clear();
  average.clear();
  gbest[0] = 0;
  gbest[1] = 0;
  best.clear();
    std::random_device rd;
    std::mt19937 engine(rd());
    std::uniform_real_distribution<> distX(-2, 2);
    std::uniform_real_distribution<> distY(-5, 5);
    std::uniform_real_distribution<> distRandom(0, 1);

    for ( int i = 0; i < swarmSize; i++){
        Particle p{};

        p.v[0] = 0;
        p.v[1] = 0;

        p.present[0] = distX(engine);
        p.present[1] = distY(engine);

        p.lbest[0] = p.present[0];
        p.lbest[1] = p.present[1];

        if ( ff.Calculate(p.lbest[0], p.lbest[1]) > ff.Calculate(gbest[0], gbest[1])){
            gbest[0] = p.lbest[0];
            gbest[1] = p.lbest[1];
        }
        particles.push_back(p);
    }

    iterations = 20;
    for ( int i = 0; i < iterations; i++){

        for ( int j = 0; j < swarmSize; j++){

            double fitnessCurrent = ff.Calculate(particles[j].present[0], particles[j].present[1]);
            double fitnesslbest = ff.Calculate(particles[j].lbest[0], particles[j].lbest[1]);
            double fitnessgbest = ff.Calculate(gbest[0], gbest[1]);
            if ( fitnessCurrent > fitnesslbest){

                particles[j].lbest[0] = particles[j].present[0];
                particles[j].lbest[1] = particles[j].present[1];
            }
            fitnesslbest = ff.Calculate(particles[j].lbest[0], particles[j].lbest[1]);
            if ( fitnesslbest > fitnessgbest){
                gbest[0] = particles[j].lbest[0];
                gbest[1] = particles[j].lbest[1];
            }
        }

        for ( int k = 0; k < swarmSize; k++){
            particles[k].v[0] = particles[k].v[0] + c1*distRandom(engine)*(particles[k].lbest[0]-particles[k].present[0])
                                     + c2*distRandom(engine)*(gbest[0] - particles[k].present[0]);

            particles[k].v[1] = particles[k].v[1] + c1*distRandom(engine)*(particles[k].lbest[1]-particles[k].present[1])
                                     + c2*distRandom(engine)*(gbest[1] - particles[k].present[1]);

            particles[k].present[0] += particles[k].v[0];
            particles[k].present[1] += particles[k].v[1];

            if ( particles[k].present[0] < -2 ){
                particles[k].present[0] = -2;
            }

            else if ( particles[k].present[0] > 2 ){
                particles[k].present[0] = 2;
            }

            if ( particles[k].present[1] < -5 ){
                particles[k].present[1] = -5;
            }
            else if ( particles[k].present[1] > 5 ){
                particles[k].present[1] = 5;
            }
        }

       double sum = std::accumulate(particles.begin(), particles.end(), 0, [](const double sum, const Particle &p){
           Fitness ff;
           return sum+ff.Calculate(p.lbest[0], p.lbest[1]);
       });
       best.push_back(ff.Calculate(gbest[0], gbest[1]));
       average.push_back(sum/swarmSize);
    }
}
