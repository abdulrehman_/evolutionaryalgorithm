#include "fitnessfunction.h"
#include <cmath>
#include <cassert>

void FitnessFunction::setFunction(const Function &value)
{
    function = value;

}

FitnessFunction::Function FitnessFunction::getFunction() const
{
    return function;
}

FitnessFunction::FitnessFunction()
{

}

double FitnessFunction::Calculate(Individual chromosome)
{
    switch (function) {
     case X2Y2:
        return chromosome.getX()*chromosome.getX() + chromosome.getY()*chromosome.getY();
        break;
    case RosenBrock:
       return std::pow(1-chromosome.getX(),2) + std::pow(100*(chromosome.getY() - std::pow(chromosome.getX(),2)),2);
        break;
    case HimmelBlau:
       return std::pow((std::pow(chromosome.getX(), 2) + chromosome.getY() - 11), 2) + std::pow(chromosome.getX() + std::pow(chromosome.getY(),2) - 7 ,2) ;
        break;
    default:
        break;
    }

}
