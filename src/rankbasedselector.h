#ifndef RANKBASEDSELECTOR_H
#define RANKBASEDSELECTOR_H

#include "iselector.h"
#include <map>
#include <cassert>
#include "interval.h"

class RankBasedSelector : public ISelector
{
    std::map<Interval, int, IntervalComparator> probabilityIndex;
    std::vector<std::pair<Individual, double>> data;
    double totalFitness = 0;
public:
    RankBasedSelector();

    // ISelector interface
public:
    void setData(const std::vector<std::pair<Individual, double> > &value);
    void Calculate();
    std::vector<Individual> Select();
};

#endif // RANKBASEDSELECTOR_H
