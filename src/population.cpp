#include "population.h"


int Population::getSize() const
{
    return size;
}

double Population::getAverage() const
{
    return totalFitness/individuals.size();
}

double Population::getBest() const
{
    return individuals[0].second;
}

FitnessFunction Population::getFitnessFunction() const
{
    return fitnessFunction;
}

void Population::setFitnessFunction(const FitnessFunction &value)
{
    fitnessFunction = value;
}

std::vector<std::pair<Individual, double> > Population::getIndividuals() const
{
    return individuals;
}

Population::Population(const Population &obj):
    size(obj.size),
    totalFitness(obj.totalFitness),
    fitnessFunction(obj.fitnessFunction),
    minX(obj.minX),
    minY(obj.minY),
    maxX(obj.maxX),
    maxY(obj.maxY)
{
    auto objIndividuals = obj.getIndividuals();

    std::copy(objIndividuals.begin(), objIndividuals.end(), std::back_inserter(individuals));
}

Population::Population(int size):size(size), totalFitness(0.0)
{
    individuals.reserve(size);
}

void Population::Initialize(double minX, double minY, double maxX, double maxY)
{
    individuals.clear();

    this->minX = minX;
    this->minY = minY;
    this->maxX = maxX;
    this->maxY = maxY;

    std::random_device rd;
    std::mt19937_64 ed(rd());


    std::uniform_real_distribution<> distX(minX, maxX);
    std::uniform_real_distribution<> distY(minY, maxY);
   for (int i = 0; i < size; i++){
       individuals.push_back(std::make_pair(Individual(true, distX(ed), minX, maxX, distY(ed), minY, maxY), 0));
   }

   totalFitness = 0;

   for ( unsigned int i = 0; i < individuals.size(); i++){
       std::pair<Individual, double> &chromosome = individuals[i];
       chromosome.second = fitnessFunction.Calculate(chromosome.first);
       totalFitness += chromosome.second;
   }

}

void Population::Evaluate()
{
   totalFitness = 0;
   for ( unsigned int i = 0; i < individuals.size(); i++){
       std::pair<Individual, double> &chromosome = individuals[i];
       chromosome.second = fitnessFunction.Calculate(chromosome.first);
       totalFitness += chromosome.second;

   }
}
void Population::Insert(std::vector<Individual> &offSpring)
{
    for (auto i: offSpring){
        individuals.push_back(std::make_pair(Individual(true, i.getX(), minX, maxX, i.getY(), minY, maxY), fitnessFunction.Calculate(i)));
    }
}

void Population::KeepFittest()
{
    std::sort(individuals.begin(), individuals.end(), [] (const std::pair<Individual, double> &a, const std::pair<Individual, double> &b) -> bool
    {
        return a.second > b.second; }
    );

    std::unique(individuals.begin(), individuals.end(), [](const std::pair<Individual, double> &a, const std::pair<Individual, double> &b) -> bool
    {
        return a.second == b.second; }
    );
    individuals.erase(individuals.begin()+size, individuals.end());
    std::cout << individuals.size();
}

void Population::PrintPopulation()
{
    for ( auto i: individuals)
        std::cout << i.first.toString() << " f(x,y) = " << i.second << std::endl;
}

void Population::WriteHeader(std::__cxx11::string path)
{
std::ofstream file;
file.open(path, std::ios_base::out | std::ios_base::trunc);
file << "<!DOCTYPE HTML>"
        "<html>"
        "<head>"
        "<title>Evolutionary Algorithm Graph</title>"

        "<style>"
        "body {font: 10pt arial;}"
        "</style>"

        "<script type=\"text/javascript\" src=\"vis.js\"></script>"
        "<script type=\"text/javascript\">";
}

void Population::WriteToFile(std::string path, int number)
{
    std::ofstream file;
    file.open(path, std::ios_base::app | std::ios_base::ate);

    file << "function drawVisualization" + std::to_string(number) + "(){"
         <<  "var data = new vis.DataSet();";
    for ( unsigned int i = 0; i < individuals.size(); i++){

         file<< "data.add({"
             << "x: " << individuals[i].first.getX() << ","
             << "y: " << individuals[i].first.getY() << ","
             << "z: " << individuals[i].second << ","
             << "style: " << individuals[i].second
             << "});" << std::endl;
    }
    file <<   "var options = {"
              "width:  '600px',"
              "height: '600px',"
              "style: 'dot-color',"
              "showPerspective: true,"
              "showGrid: true,"
              "showShadow: false,"
              "keepAspectRatio: true,"
              "verticalRatio: 0.5"
            "};"
            "var container = document.getElementById('mygraph" << number <<"');"
            "document.getElementById('best" << number << "').innerHTML = \"Best: "<< individuals[0].second <<"\";"
            "document.getElementById('avg" << number  << "').innerHTML = \"Average: " << totalFitness/individuals.size() <<"\";"
            "graph3d = new vis.Graph3d(container, data, options);"
          "}";

    file.close();
}

void Population::WriteFooter(std::__cxx11::string path, int size)
{
    std::ofstream file;
    file.open(path, std::ios_base::app | std::ios_base::ate);
    file << "function drawVisualization(){";
    for ( int i = 0; i < size; i++){
      file << "drawVisualization" << i <<"();";
    }
    file <<  "}</script>"
            "</head>"
            "<body onload=\"drawVisualization();\">";
      for ( int i = 0; i < size; i++){
        file << "<div id=\"mygraph" << i <<"\"></div>"
             << "<div> "
             << "<h2 id=\"best"<< i <<"\">"
             << "</h2>"
             << "<h2 id=\"avg" << i <<"\">"
             << "</h2>"
             << "</div>";
      }
       file << "</body>"
               "</html>";
  file.close();
}
