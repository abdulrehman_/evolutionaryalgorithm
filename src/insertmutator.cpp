#include "insertmutator.h"

double InsertMutator::getMax() const
{
    return max;
}

void InsertMutator::setMax(double value)
{
    max = value;
}

double InsertMutator::getMin() const
{
    return min;
}

void InsertMutator::setMin(double value)
{
    min = value;
}

InsertMutator::InsertMutator(double min, double max)
    :min(min), max(max)
{

}

void InsertMutator::Mutate(std::vector<Individual> &offSpring)
{
    std::mt19937_64 ed(std::chrono::high_resolution_clock::now().time_since_epoch().count());
    std::uniform_real_distribution<> dist(min, max);

    for ( int i = 0; i < offSpring.size(); i++){
        offSpring[i].setX(offSpring[i].getX() + dist(ed));
        offSpring[i].setY(offSpring[i].getY() + dist(ed));
    }
}
