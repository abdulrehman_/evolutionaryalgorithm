#-------------------------------------------------
#
# Project created by QtCreator 2016-02-14T15:13:13
#
#-------------------------------------------------

QT += widgets printsupport

TARGET = EvolutionaryAlgorithmAssignment
TEMPLATE = app

QMAKE_CXXFLAGS = -std=c++14

SOURCES += main.cpp\
    fitnessproportionateselector.cpp \
    insertmutator.cpp \
    fitnessfunction.cpp \
    evolutionaryalgorithm.cpp \
    population.cpp \
    individual.cpp \
    mainwindow.cpp \
    qcustomplot.cpp \
    imutator.cpp \
    swapmutator.cpp \
    iselector.cpp \
    icrossoverstrategy.cpp \
    onepointcrossover.cpp \
    htmlwriter.cpp \
    algorithmsettingdialog.cpp \
    rankbasedselector.cpp \
    binarytournamentselector.cpp \
    fitness.cpp \
    particle.cpp \
    swarm.cpp


HEADERS  += \
    fitnessproportionateselector.h \
    insertmutator.h \
    fitnessfunction.h \
    evolutionaryalgorithm.h \
    population.h \
    individual.h \
    mainwindow.h \
    qcustomplot.h \
    imutator.h \
    swapmutator.h \
    iselector.h \
    icrossoverstrategy.h \
    onepointcrossover.h \
    htmlwriter.h \
    algorithmsettingdialog.h \
    rankbasedselector.h \
    binarytournamentselector.h \
    interval.h \
    fitness.h \
    particle.h \
    swarm.h


FORMS += \
    mainwindow.ui \
    algorithmsettingdialog.ui
