#ifndef INDIVIDUAL_H
#define INDIVIDUAL_H

#include <string>
#include <stdexcept>

class Individual
{
private:

    double x;
    double y;

    const double minX;
    const double maxX;

    const double minY;
    const double maxY;

public:
    Individual(bool trunc, double x, double minX, double maxX, double y, double minY, double maxY);
    Individual& operator=(const Individual &other){
        if ( minX == other.getMinX() && maxX == other.getMaxX()
           && minY == other.getMinY() && maxY == other.getMaxY()){
        x = other.getX();
        y = other.getY();
        }
        else
            throw std::out_of_range("Ranges are not equal");
        return *this;
    }
    friend bool operator==(const Individual &first, const Individual &second);

    double getX() const;
    void setX(double value);

    double getY() const;
    void setY(double value);

    double getMinX() const;

    double getMaxX() const;

    double getMinY() const;

    double getMaxY() const;

    std::string toString() const;
};

#endif // INDIVIDUAL_H
