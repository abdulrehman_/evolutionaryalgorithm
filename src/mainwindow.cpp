#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setFixedSize(this->size());

    selectedGraph = 0;
    iterationSize = settings.value("Population/iteration").toInt();
    generationSize = settings.value("Population/generation").toInt();

    connect(ui->action_Quit, &QAction::triggered, this, &MainWindow::close);
    connect(ui->actionShow_3D_graph, &QAction::triggered, this, &MainWindow::Open3DGraphOnBrowser);
    connect(ui->actionPSO, &QAction::triggered, this, &MainWindow::EnablePSO);
    connect(ui->actionExport, &QAction::triggered, this, &MainWindow::Export);
    connect(ui->executeButton, &QPushButton::clicked, this, &MainWindow::Execute);
    connect(ui->nextButton, &QPushButton::clicked, this, &MainWindow::Next);
    connect(ui->previousButton, &QPushButton::clicked, this, &MainWindow::Previous);
    connect(ui->action_Algorithm_Settings, &QAction::triggered, this, &MainWindow::algorithmSettings);
    connect(ui->actionLegend, &QAction::triggered, this, &MainWindow::ShowHideLegend);

    ui->widget->addGraph();
    ui->widget->addGraph();
    ui->widget->legend->setVisible(true);

    // give the axes some labels:

    ui->widget->xAxis->setLabel("Generation");
    ui->widget->yAxis->setLabel("Fitness");


    ui->widget->graph(0)->setPen(QPen(Qt::red));
    ui->widget->graph(0)->setName("EA Best");


    //ui->widget->graph(1)->setData(x, y);
    ui->widget->graph(1)->setName("EA Average");
    //ui->
    ui->widget->replot();
 //   plotTitle->setText("Generation 2");

    iterationSize = settings.value("Population/iteration").toInt();
    generationSize = settings.value("Population/generation").toInt();

    for ( int i = 0; i < generationSize; i++){
        generations.push_back(i);
    }

    for ( int i = 0; i < iterationSize; i++){
        iterations.push_back(i);
    }

   ui->widget->replot();
    // set axes ranges, so we see all data:
    // delete plotTitle;

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::Execute()
{
    fitnessAvgCollection.clear();
    fitnessBestCollection.clear();
    psoFitnessAvgCollection.clear();
    psoFitnessBestCollection.clear();
    psoFitnessAvgAvg.clear();
    psoFitnessAvgBest.clear();

    FitnessFunction ff;
     ff.setFunction(static_cast<FitnessFunction::Function>(settings.value("FitnessFunction/index").toInt()));
    InsertMutator im(settings.value("Mutation/minimumValue").toDouble(), settings.value("Mutation/maximumValue").toDouble());;
    SwapMutator sm{};
    FitnessProportionateSelector fps{};
    RankBasedSelector rbs{};
    BinaryTournamentSelector bts{};

    IMutator *mutator = nullptr;
    ISelector *selector = nullptr;
    OnePointCrossover opc;

    int MutationIndex = settings.value("Mutation/type").toInt();
    if ( MutationIndex == 0)
        mutator = &im;
    else
        mutator = &sm;

    int SelectionType = settings.value("Population/selectionType").toInt();
    if ( SelectionType == 0){
        selector = &fps;
    }else if ( SelectionType == 1){
        selector = &rbs;
    }else{
        selector = &bts;
    }

    EvolutionaryAlgorithm Algorithm(ff, *mutator, opc, *selector, settings.value("Population/size").toInt());

    Algorithm.setGeneration(settings.value("Population/generation").toInt());
    Algorithm.setMutationEnabled(settings.value("Mutation/isAllow").toBool());
    Algorithm.setMutationProbability(settings.value("Mutation/probability").toDouble());
    Algorithm.setBreedRate(settings.value("Crossover/breedRate").toDouble());

    Algorithm.setLimitMinX(settings.value("Limit/minX").toInt());
    Algorithm.setLimitMinY(settings.value("Limit/minY").toInt());

    Algorithm.setLimitMaxX(settings.value("Limit/maxX").toInt());
    Algorithm.setLimitMaxY(settings.value("Limit/maxY").toInt());

    ui->label->setText(QString::number(settings.value("Population/iteration").toInt()+1));

    ui->progressBar->setValue(0);
    selectedGraph = 0;
    ui->spinBox->setValue(selectedGraph+1);

    iterationSize = settings.value("Population/iteration").toInt();
    generationSize = settings.value("Population/generation").toInt();

    Swarm PSO(generationSize);

    for ( int i = 0; i < iterationSize; i++){



          Algorithm.Execute();
          PSO.Execute();

          auto x = Algorithm.getBestAvgVector();

          fitnessBest.clear();
          fitnessAvg.clear();

          psoFitnessAvg.clear();
          psoFitnessBest.clear();

          std::transform(x.begin(), x.end(), std::back_inserter(fitnessBest), [](const std::pair<double, double> &p){
              return p.first;
          });
          std::transform(x.begin(), x.end(), std::back_inserter(fitnessAvg), [](const std::pair<double, double> &p){
              return p.second;
          });


        fitnessAvgCollection.push_back(fitnessAvg);
        fitnessBestCollection.push_back(fitnessBest);


        if ( ui->actionPSO->isChecked()){
            psoFitnessAvg = QVector<double>::fromStdVector(PSO.average);
            psoFitnessBest = QVector<double>::fromStdVector(PSO.best);
            psoFitnessAvgCollection.push_back(psoFitnessAvg);
            psoFitnessBestCollection.push_back(psoFitnessBest);

            double sumMin = 0;

            sumMin = std::accumulate(psoFitnessAvg.begin(), psoFitnessAvg.end(), 0);
            psoFitnessAvgAvg.push_back(sumMin/psoFitnessAvg.size());

            double sumMax = 0;
            sumMax = std::accumulate(psoFitnessBest.begin(), psoFitnessBest.end(), 0);

            psoFitnessAvgBest.push_back(sumMax/psoFitnessBest.size());
        }
        double sumMin = 0;

        sumMin = std::accumulate(fitnessAvg.begin(), fitnessAvg.end(), 0);
        fitnessAvgAvg.push_back(sumMin/fitnessAvg.size());

        double sumMax = 0;
        sumMax = std::accumulate(fitnessBest.begin(), fitnessBest.end(), 0);

        fitnessAvgBest.push_back(sumMax/fitnessBest.size());


    ui->progressBar->setValue(i/static_cast<double>(iterationSize)*100.0);
    }
    ui->progressBar->setValue(100);
         //double sum = std::accumulate(fitnessAvg.begin(), fitnessAvg.end(), 0.0);

         //fitnessBestBest.push_back(fitnessBest.size()-1);
         //fitnessAvgBest.push_back(fitnessAvg.size()-1);

         ui->widget->graph(0)->setData(generations, fitnessBestCollection[0]);
         ui->widget->graph(1)->setData(generations, fitnessAvgCollection[0]);

         if (ui->actionPSO->isChecked()){
             ui->widget->graph(2)->setData(generations, psoFitnessBestCollection[0]);
             ui->widget->graph(3)->setData(generations, psoFitnessAvgCollection[0]);
         }
         ui->widget->rescaleAxes();
         ui->widget->replot();


}

void MainWindow::Next()
{
    selectedGraph++;

    if ( selectedGraph > iterationSize - 1+1){
        selectedGraph = iterationSize - 1+1;
    }
    ui->spinBox->setValue(selectedGraph+1);
    if ( selectedGraph < iterationSize){

    std::cout << "clicked";
  //  ui->widget->plotLayout()->removeAt(0);
  //  ui->widget->plotLayout()->insertRow(0);
  //  ui->widget->setLayout(&plotLayout);
  //  ui->widget->plotLayout()->insertRow(0);

 //  plotTitle = new QCPPlotTitle(ui->widget, "Iteration " +QString::number(selectedGraph+1));
    // ui->widget->plotLayout()->addElement(0, 0, plotTitle);


   // plotTitle->setText("Iteration " +(selectedGraph+1));
   // plotTitle->setText("Iteration " +(selectedGraph+1));
   //  ui->widget->plotLayout()->remove(plotTitle);
   //  ui->widget->setL
    ui->widget->xAxis->setLabel("Generation");
    ui->widget->yAxis->setLabel("Fitness");

    ui->widget->graph(0)->setData(generations, fitnessBestCollection[selectedGraph]);
    ui->widget->graph(1)->setData(generations, fitnessAvgCollection[selectedGraph]);

    if (ui->actionPSO->isChecked()){
     ui->widget->graph(2)->setData(generations, psoFitnessBestCollection[selectedGraph]);
     ui->widget->graph(3)->setData(generations, psoFitnessAvgCollection[selectedGraph]);
    }
    }
    else{
     ui->widget->xAxis->setLabel("Iterations");
     ui->widget->yAxis->setLabel("Fitness Averages");

     ui->widget->graph(0)->setData(iterations, fitnessAvgBest);
     ui->widget->graph(1)->setData(iterations, fitnessAvgAvg);
    if (ui->actionPSO->isChecked()){
     ui->widget->graph(2)->setData(iterations, psoFitnessAvgBest);
     ui->widget->graph(3)->setData(iterations, psoFitnessAvgAvg);
    }
    }
       ui->widget->rescaleAxes();
    ui->widget->replot();
}

void MainWindow::Previous()
{
   selectedGraph--;
   if ( selectedGraph < 0)
       selectedGraph = 0;

  // ui->widget->plotLayout()->removeAt(0);
  // ui->widget->plotLayout()->insertRow(0);
//
  //plotTitle = new QCPPlotTitle(ui->widget, "Iteration " +QString::number(selectedGraph+1));
//    ui->widget->plotLayout()->addElement(0, 0, plotTitle);
  // plotTitle->setText("Iteration " +(selectedGraph+1));
  //  ui->widget->plotLayout()->updateLayout();
   ui->widget->xAxis->setLabel("Generation");
   ui->widget->yAxis->setLabel("Fitness");

   ui->spinBox->setValue(selectedGraph+1);
   ui->widget->graph(0)->setData(generations, fitnessBestCollection[selectedGraph]);
   ui->widget->graph(1)->setData(generations, fitnessAvgCollection[selectedGraph]);
   if (ui->actionPSO->isChecked()){
    ui->widget->graph(2)->setData(generations, psoFitnessBestCollection[selectedGraph]);
    ui->widget->graph(3)->setData(generations, psoFitnessAvgCollection[selectedGraph]);
   }


   ui->widget->rescaleAxes();

   ui->widget->replot();


}

void MainWindow::algorithmSettings()
{
    AlgorithmSettingDialog *x = new AlgorithmSettingDialog(this);
    x->GetStarted();
    ui->label->setText(QString::number(settings.value("Population/iteration").toInt()+1));

    delete x;
}

void MainWindow::Open3DGraphOnBrowser()
{
    QDesktopServices::openUrl(QUrl("./data.html"));
}

void MainWindow::ShowHideLegend(bool value)
{
    if ( value)
        ui->widget->legend->setVisible(true);
    else
        ui->widget->legend->setVisible(false);

    ui->widget->replot();
}

void MainWindow::EnablePSO(bool value)
{
    if (value){
        ui->widget->addGraph();
        ui->widget->addGraph();

        ui->widget->graph(2)->setPen(QPen(Qt::green));
        ui->widget->graph(2)->setName("PSO Best");

        ui->widget->graph(3)->setPen(QPen(Qt::cyan));
        ui->widget->graph(3)->setName("PSO Average");
    }
    else{
        ui->widget->removeGraph(3);
        ui->widget->removeGraph(2);
    }
    ui->widget->replot();
}
void MainWindow::Export()
{

    QFileDialog dialog(this);
    dialog.setWindowModality(Qt::WindowModal);
    dialog.setAcceptMode(QFileDialog::AcceptSave);
    QStringList files;
    if (dialog.exec()){
        files = dialog.selectedFiles();


    QFile file(files.at(0));
       if (!file.open(QFile::WriteOnly | QFile::Text)) {
           QMessageBox::warning(this, tr("Application"),
                                tr("Cannot write file %1:\n%2.")
                                .arg(files.at(0))
                                .arg(file.errorString()));
       }
       else
       {
       QTextStream out(&file);
       QStringList strList;

           strList <<
                   "\" Evolutionary Algorithm\" ";


           if (ui->actionPSO->isChecked())
           strList <<  "\" PSO\" ";

            out << strList.join(",") + "\n";

          for ( int i = 0; i < generationSize; i++){
               strList.clear();


                  strList <<
                          "\" " +
                          QString::number(fitnessAvg[i]) +
                          "\" ";
                   if (ui->actionPSO->isChecked()){
                  strList <<
                          "\" " +
                          QString::number(psoFitnessAvg[i]) +
                          "\" ";
                    }


              out << strList.join(",") + "\n";
          }
       }
    }
}
