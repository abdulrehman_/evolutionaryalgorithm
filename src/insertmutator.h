#ifndef INSERTMUTATOR_H
#define INSERTMUTATOR_H

#include "imutator.h"
#include <chrono>
#include <random>

class InsertMutator : public IMutator
{
   double min;
   double max;
public:
    InsertMutator(double min, double max);

    // IMutator interface
public:
    void Mutate(std::vector<Individual> &offSpring);

    double getMax() const;
    void setMax(double value);

    double getMin() const;
    void setMin(double value);
};

#endif // INSERTMUTATOR_H
