#include "individual.h"


double Individual::getX() const
{
    return x;
}

void Individual::setX(double value)
{
    if ( value < minX)
        value = minX;
    if ( value > maxX)
        value = maxX;

    x = value;
}

double Individual::getY() const
{

    return y;
}

void Individual::setY(double value)
{
    if ( value < minY)
        value = minY;
    if ( value > maxY)
        value = maxY;
    y = value;
}

double Individual::getMinX() const
{
    return minX;
}

double Individual::getMaxX() const
{
    return maxX;
}

double Individual::getMinY() const
{
    return minY;
}

double Individual::getMaxY() const
{
    return maxY;
}

std::string Individual::toString() const
{
    return std::string( "x(" + std::to_string(minX) + " <= " + std::to_string(x) + " <= " + std::to_string(maxX) +")"+
                       ", y(" + std::to_string(minY) + " <= " + std::to_string(y) + " <= " + std::to_string(maxY) + ")");
}

Individual::Individual(bool trunc, double x, double minX, double maxX, double y, double minY, double maxY)
    :x(x), y(y), minX(minX), maxX(maxX), minY(minY), maxY(maxY){


    if ( minX >= maxX)
        throw std::invalid_argument("minimum of X must be less than " + std::to_string(maxX));

    if ( minY >= maxY)
        throw std::invalid_argument("minimum of Y must be less than " + std::to_string(maxY));

    if (trunc){
        if ( x < minX)
            x = minX;
        if ( x > maxX)
            x = maxX;
        if ( y < minY)
            y = minY;
        if ( y > maxY)
            y = maxY;
    }
    else
   {
    if ( x < minX || x > maxX)
    throw std::invalid_argument(std::string("Value of X must be between " + std::to_string(minX) + " and " + std::to_string(maxX)));

    if ( y < minY || y > maxY)
    throw std::invalid_argument(std::string("Value of X must be between " + std::to_string(minY) + " and " + std::to_string(maxY)));
    }
}

bool operator==(const Individual &first, const Individual &other)
{

       return first.x == other.x && first.y == other.y;

}
