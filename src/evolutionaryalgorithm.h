#ifndef EVOLUTIONARYALGORITHM_H
#define EVOLUTIONARYALGORITHM_H

#include "fitnessfunction.h"
#include "imutator.h"
#include "icrossoverstrategy.h"
#include "onepointcrossover.h"
#include "insertmutator.h"
#include "swapmutator.h"
#include "population.h"
#include "iselector.h"
#include <fstream>


class EvolutionaryAlgorithm
{
private:
    FitnessFunction     fitnessFunction;
    IMutator            *mutator;
    ICrossoverStrategy  *crossoverStrategy;
    ISelector           *selector;
    bool                isTerminated;
    Population          population;
    std::vector<std::pair<double, double>> bestAvgVector;

    bool mutationEnabled;
    double mutationProbability;
    double breedRate;
    int generation;

    int limitMinX;
    int limitMinY;

    int limitMaxX;
    int limitMaxY;

public:
    enum MutatorType { Insert, Swap };
    EvolutionaryAlgorithm();
    EvolutionaryAlgorithm(FitnessFunction &fitnessFunction,
                          IMutator &mutator,
                          ICrossoverStrategy &crossoverStrategy,
                          ISelector &selector,
                          int populationSize);

    void SetMutatorType(MutatorType mutator);
    void Execute();

    std::vector<std::pair<double, double> > getBestAvgVector() const;

    bool getMutationEnabled() const;
    void setMutationEnabled(bool value);

    double getMutationProbability() const;
    void setMutationProbability(double value);

    double getBreedRate() const;
    void setBreedRate(double value);

    int getGeneration() const;
    void setGeneration(int value);

    int getLimitMinX() const;
    void setLimitMinX(int value);

    int getLimitMinY() const;
    void setLimitMinY(int value);

    int getLimitMaxX() const;
    void setLimitMaxX(int value);

    int getLimitMaxY() const;
    void setLimitMaxY(int value);
};

#endif // EVOLUTIONARYALGORITHM_H
