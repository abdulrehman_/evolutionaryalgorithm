#include "evolutionaryalgorithm.h"
#include <cassert>

std::vector<std::pair<double, double> > EvolutionaryAlgorithm::getBestAvgVector() const
{
    return bestAvgVector;
}

bool EvolutionaryAlgorithm::getMutationEnabled() const
{
    return mutationEnabled;
}

void EvolutionaryAlgorithm::setMutationEnabled(bool value)
{
    mutationEnabled = value;
}

double EvolutionaryAlgorithm::getMutationProbability() const
{
    return mutationProbability;
}

void EvolutionaryAlgorithm::setMutationProbability(double value)
{
    mutationProbability = value;
}

double EvolutionaryAlgorithm::getBreedRate() const
{
    return breedRate;
}

void EvolutionaryAlgorithm::setBreedRate(double value)
{
    breedRate = value;
}

int EvolutionaryAlgorithm::getGeneration() const
{
    return generation;
}

void EvolutionaryAlgorithm::setGeneration(int value)
{
    generation = value;
}

int EvolutionaryAlgorithm::getLimitMinX() const
{
    return limitMinX;
}

void EvolutionaryAlgorithm::setLimitMinX(int value)
{
    limitMinX = value;
}

int EvolutionaryAlgorithm::getLimitMinY() const
{
    return limitMinY;
}

void EvolutionaryAlgorithm::setLimitMinY(int value)
{
    limitMinY = value;
}

int EvolutionaryAlgorithm::getLimitMaxX() const
{
    return limitMaxX;
}

void EvolutionaryAlgorithm::setLimitMaxX(int value)
{
    limitMaxX = value;
}

int EvolutionaryAlgorithm::getLimitMaxY() const
{
    return limitMaxY;
}

void EvolutionaryAlgorithm::setLimitMaxY(int value)
{
    limitMaxY = value;
}

EvolutionaryAlgorithm::EvolutionaryAlgorithm(FitnessFunction &fitnessFunction,
                                             IMutator &mutator,
                                             ICrossoverStrategy &crossoverStrategy,
                                             ISelector &selector,
                                             int populationSize)
    :fitnessFunction(fitnessFunction),
     population(populationSize)
{
 this->mutator = &mutator;
 this->crossoverStrategy = &crossoverStrategy;
 this->selector = &selector;
}



void EvolutionaryAlgorithm::Execute()
{

     population.WriteHeader("./data.html");
     population.setFitnessFunction(fitnessFunction);
     population.Initialize(limitMinX, limitMinY, limitMaxX, limitMaxY);

     selector->setData(population.getIndividuals());
     selector->Calculate();

     bestAvgVector.clear();
     for ( int i = 0; i < generation; i++)
     {
      //  std::cout << "------------------------------------------------------------" << std::endl;
      //  std::cout << "population: " << i << std::endl;
      //  population.PrintPopulation();

          population.WriteToFile("./data.html", i);
        for ( int j = 0; j < breedRate*population.getSize(); j++){

            std::vector<Individual> parents = selector->Select();
          //  std::vector<Individual> parents = population.SelectParent();
            std::vector<Individual> offSpring = crossoverStrategy->Crossover(parents);
            assert(!parents.empty());
            assert(!offSpring.empty());
            if ( mutationEnabled){
            std::random_device rd;
            std::mt19937_64 ed(rd());
            std::uniform_real_distribution<> dist(0, 1);

            if ( dist(ed) < mutationProbability)
            mutator->Mutate(offSpring);
            }
            population.Insert(offSpring);
        }
       population.KeepFittest();
       population.Evaluate();

       selector->setData(population.getIndividuals());
       selector->Calculate();
       bestAvgVector.push_back(std::make_pair(population.getBest(), population.getAverage()));
      // population.PrintPopulation();
     }
 population.WriteFooter("./data.html", population.getSize());
}

