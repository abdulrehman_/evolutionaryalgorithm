#ifndef ISELECTOR_H
#define ISELECTOR_H

#include <vector>
#include "individual.h"
#include <random>


class ISelector
{
public:
    virtual void setData(const std::vector<std::pair<Individual, double> > &value) = 0;
    virtual void Calculate() = 0;
    virtual std::vector<Individual> Select() = 0;
    virtual ~ISelector(){

   }
};

#endif // ISELECTOR_H
