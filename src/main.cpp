#include <iostream>
#include <vector>
#include <QApplication>
#include <QSettings>

#include "mainwindow.h"



#include "evolutionaryalgorithm.h"
#include "onepointcrossover.h"
#include "insertmutator.h"
#include "swapmutator.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setOrganizationName("Unbounded");
    QCoreApplication::setOrganizationDomain("unbounded");
    QCoreApplication::setApplicationName("EvolutionaryAlgorithm");

   QApplication a(argc, argv);
   MainWindow w;
   w.show();



    return a.exec();
}
