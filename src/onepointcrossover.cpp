#include "onepointcrossover.h"

#include <cassert>

OnePointCrossover::OnePointCrossover()
{

}

std::vector<Individual> OnePointCrossover::Crossover(std::vector<Individual> &parents)
{
    Individual offSpringA(true, parents[0].getX(), parents[0].getMinX(), parents[0].getMaxX(),
                                parents[1].getY(), parents[1].getMinY(), parents[1].getMaxY());

    Individual offSpringB(true, parents[1].getX(), parents[1].getMinX(), parents[1].getMaxX(),
                                parents[0].getY(), parents[0].getMinY(), parents[0].getMaxY());

    std::vector<Individual> offSpring;
    offSpring.push_back(offSpringA);
    offSpring.push_back(offSpringB);

    return offSpring;
}
