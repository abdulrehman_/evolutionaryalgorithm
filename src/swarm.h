#ifndef SWARM_H
#define SWARM_H

#include <vector>
#include "swarm.h"
#include "particle.h"
#include "fitness.h"
#include <random>
#include <iostream>
#include <algorithm>

class Swarm
{
    std::vector<Particle> particles;
    std::vector<double> gbest;


    Fitness ff;
    int swarmSize;
    int iterations;
    int c1;
    int c2;

public:
    Swarm();
    Swarm(int swarmSize);

    void Execute();

    std::vector<Particle> getParticles() const;
    void setParticles(const std::vector<Particle> &value);

    std::vector<double> getGbest() const;
    void setGbest(const std::vector<double> &value);

    int getSwarmSize() const;

    std::vector<double> best;
    std::vector<double> average;

};

#endif // SWARM_H
