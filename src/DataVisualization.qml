import QtQuick 2.5
import QtQuick.Window 2.2

Window {
    visible: true

    DataVisualizationForm {
        anchors.fill: parent
        mouseArea.onClicked: {
            Qt.quit();
        }
    }
}
