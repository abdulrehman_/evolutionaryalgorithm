#ifndef HTMLWRITER_H
#define HTMLWRITER_H

#include <fstream>
#include "individual.h"
#include <vector>

class HTMLWriter
{
    std::ofstream file;
    std::vector<std::pair<Individual, double>> data;
public:
    explicit HTMLWriter(std::string path, const std::vector<std::pair<Individual, double> > &data);
    ~HTMLWriter();
    void WriteHeader();
    void WriteContent(int contentId);
    void WriteFooter();
    void setData(std::vector<std::pair<Individual, double> > &value);
};

#endif // HTMLWRITER_H
