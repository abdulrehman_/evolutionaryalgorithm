#ifndef AlgorithmSETTINGDIALOG_H
#define AlgorithmSETTINGDIALOG_H

#include <QDialog>
#include <QSettings>

namespace Ui {
class AlgorithmSettingDialog;
}

class AlgorithmSettingDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AlgorithmSettingDialog(QWidget *parent = 0);
    ~AlgorithmSettingDialog();
    void GetStarted();
private:
    Ui::AlgorithmSettingDialog *ui;
    QSettings settings;
};

#endif // AlgorithmSETTINGDIALOG_H
