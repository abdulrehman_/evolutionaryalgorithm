#include "particle.h"

std::vector<double> Particle::getV() const
{
    return v;
}

void Particle::setV(const std::vector<double> &value)
{
    v = value;
}

std::vector<double> Particle::getPresent() const
{
    return present;
}

void Particle::setPresent(const std::vector<double> &value)
{
    present = value;
}

std::vector<double> Particle::getLbest() const
{
    return lbest;
}

void Particle::setLbest(const std::vector<double> &value)
{
    lbest = value;
}

Particle::Particle():v(2), present(2), lbest(2)
{

}
