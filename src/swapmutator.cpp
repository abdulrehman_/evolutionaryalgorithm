#include "swapmutator.h"

SwapMutator::SwapMutator()
{

}

void SwapMutator::Mutate(std::vector<Individual> &offSpring)
{
    for (unsigned int i = 0; i < offSpring.size(); i++){
        double x = offSpring[i].getX();
        double y = offSpring[i].getY();

        offSpring[i].setY(x);
        offSpring[i].setX(y);
    }
}
