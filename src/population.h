#ifndef POPULATION_H
#define POPULATION_H

#include <individual.h>
#include <random>
#include <iostream>
#include <vector>
#include <map>
#include <iomanip>
#include "fitnessfunction.h"
#include <algorithm>
#include <chrono>
#include <fstream>
#include "fitnessfunction.h"
#include "interval.h"


class Population
{
    const int size;
    std::vector<std::pair<Individual, double>> individuals;
    double totalFitness;
    FitnessFunction fitnessFunction;
    
    double minX;
    double minY;

    double maxX;
    double maxY;
public:
    Population( const Population &obj);
    explicit Population(int size);
    void Initialize(double minX, double minY, double maxX, double maxY);
    void Evaluate();
    void Insert(std::vector<Individual> &offSpring);
    void KeepFittest();
    // Should be separated from Population class
    void PrintPopulation();
    void WriteHeader(std::string path);
    void WriteToFile(std::string path, int number);
    void WriteFooter(std::string path, int size);
    // ------------------------------------------
    int  getSize() const;
    double getAverage() const;
    double getBest() const;
    FitnessFunction getFitnessFunction() const;
    void setFitnessFunction(const FitnessFunction &value);
    std::vector<std::pair<Individual, double> > getIndividuals() const;
};

#endif // POPULATION_H
